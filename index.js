const http = require("http");
http
  .createServer(function (req, res) {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.write(JSON.stringify({ ip: req.headers["cf-connecting-ip"] }));
    res.end();
  })
  .listen(80, "0.0.0.0");
