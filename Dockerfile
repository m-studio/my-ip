FROM node:lts-alpine

WORKDIR /usr/src/app
COPY . .

EXPOSE 80

CMD [ "node", "index.js" ]
